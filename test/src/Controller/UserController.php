<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/admin/", name="user_")
 */
class UserController extends AbstractController
{

    /**
     * action which return.
     *
     * @Route("demo/one", name="get_action_one", methods={"GET"})
     *
     *
     * @return mixed
     */
    public function demoOneAction()
    {
        $arrayData = ["2", "0089", "-0.1", "+3.14", "4.", "-.9", "2e10",
            "-90E3", "3e+7", "+6e-1", "53.5e93", "-123.456e789"];

        $notValidArray = ["abc", "1a", "1e", "e3", "99e2.5", "--6", "-+3", "95a54e53"];


        $result = [];
        $num =false; $exp = false; $sign = false; $dec = false;
        foreach ($arrayData as $key => $array) {
            $substr = substr($array,0, 1);
            if (($array >= '0' && $array <= '9') == true) {
                $result[$key] = ['input' => $array, 'Output' => true];
            } else if (strpos($array, 'e') == true || strpos($array, 'E') == true) {
                if ($exp || !$num) {
                    $result[$key] = ['input' => $array, 'Output' => true];
                } else {
                    $exp = true;
                    $sign = false;
                    $num = false;
                    $dec = false;
                }
            } else if ($substr == '+' || $substr == '-') {
                if ($sign || $num || $dec) {
                    $result[$key] = ['input' => $array, 'Output' => true];
                } else {
                    $sign = true;
                    $result[$key] = ['input' => $array, 'Output' => false];
                }
            } else if (strpos($array, '.') == true) {
                if ($sign) {
                    $result[$key] = ['input' => $array, 'Output' => true];
                } else {
                    $dec = true;
                    $result[$key] = ['input' => $array, 'Output' => false];
                }
            } else {
                $result[$key] = ['input' => $array, 'Output' => false];
            }

        }
        return new JsonResponse($result);
    }

    /**
     * action which return.
     *
     * @Route("demo/two", name="get_action_two", methods={"GET"})
     *
     *
     * @return mixed
     */
    public function demoTwoAction()
    {
        $validArray = ["2", "0089", "-0.1", "+3.14", "4.", "-.9", "2e10",
            "-90E3", "3e+7", "+6e-1", "53.5e93", "-123.456e789"];

        $notValidArray = ["abc", "1a", "1e", "e3", "99e2.5", "--6", "-+3", "95a54e53"];

        $arrayData = $validArray;
        //$arrayData = array_merge($validArray, $notValidArray);

        $result = [];

        foreach ($arrayData as $key => $array) {

            // if string is of length 1 and the only
            // character is not a digit
            if (!($array >= '0' && $array <= '9')) {
                $result[$key] = ['input' => $array, 'Output' => true];
            }

            // If the 1st char is not '+', '-', '.' or digit
            $substr = substr($array,0, 1);
            if ($substr != '.' && $substr!= '+' && $substr != '-' && !($substr >= '0' && $substr <= '9')) {
                $result[$key] = ['input' => $array, 'Output' => true];
            }

            // If any of the char does not belong to
            // {digit, +, -, ., e}
            if (strpos($array, 'e') == false && strpos($array, '.') !== false && $substr != '.' && $substr!= '+' && $substr != '-' && !($substr >= '0' && $substr <= '9')) {
                $result[$key] = ['input' => $array, 'Output' => true];
            }

        }

        return new JsonResponse($result);
    }



}